#! /usr/bin/env ruby

# Copyright: 2015 Balasankar C <balasankarc@autistici.org>
# Insiped by gemdeps from Cédric Boutillier <boutil@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# .
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# .
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'gemnasium/parser'
require 'gems'
require 'yaml'
require 'open-uri'
require 'open3'

$exceptions = { 'rake' => 'rake', 'rubyntlm' => 'ruby-ntlm', 'rails' =>  'rails', 'asciidoctor' => 'asciidoctor', 'unicorn' => 'unicorn', 'capistrano' => 'capistrano', 'cucumber' => 'cucumber',  'rubyzip' => 'ruby-zip', 'thin' => 'thin', 'racc' => 'racc', 'pry' => 'pry', 'rexical' => 'rexical', 'messagebus_ruby_api' => 'ruby-messagebus-api', 'bundler' => 'bundler', 'org-ruby' => 'ruby-org', 'CFPropertyList' => 'ruby-cfpropertylist', 'ruby-saml' => 'ruby-saml', 'ruby_parser' => 'ruby-parser', 'RedCloth' => 'ruby-redcloth', 'gitlab_omniauth-ldap' => 'ruby-omniauth-ldap', "pyu-ruby-sasl" => "ruby-sasl" }

def get_debian_name(gemname)
    # Returns the debian specific name of the package
    if $exceptions.include?(gemname) then
        return $exceptions[gemname]
    else
        hyphen_name = gemname.gsub("_","-")
        debian_name = "ruby-"+hyphen_name
        return debian_name
    end
end

def is_in_unstable(gemname)
    debian_name = get_debian_name(gemname)
    rmadison_output = `rmadison -s unstable -a amd64,all #{debian_name} 2>&1`
    puts "rmadison output is " + rmadison_output
    count = 0
    if rmadison_output.include?("curl:") then # Handle curl connectivity errors
        while rmadison_output.include?("curl:") do
            print "Retrying #"
            puts count
            count = count+1
            rmadison_output = `rmadison -s unstable -a amd64,all #{debian_name} 2>&1`
            puts "rmadison output is " + rmadison_output
        end
    end
    suite = "Unstable"
    status = "Packaged"
    begin
        version_crude = rmadison_output.split("|")[1].strip()
    rescue
        version_crude = "NA"
        suite = "Unpackaged"
        status = "Unpackaged"
    end
    return version_crude,suite,status
end

def is_in_experimental(gemname)
    debian_name = get_debian_name(gemname)
    rmadison_output = `rmadison -s experimental -a amd64,all #{debian_name} 2>&1`
    puts "rmadison output is " + rmadison_output
    count = 0
    if rmadison_output.include?("curl:") then
        while rmadison_output.include?("curl:") do
            print "Retrying #"
            puts count
            count = count+1
            rmadison_output = `rmadison -s experimental -a amd64,all #{debian_name} 2>&1`
            puts "rmadison output is " + rmadison_output
        end
    end
    suite = "Experimental"
    status = "Packaged"
    begin
        version_crude = rmadison_output.split("|")[1].strip()
    rescue 
        version_crude = "NA"
        suite = "Unpackaged"
        status = "Unpackaged"
    end
    return version_crude,suite,status
end

def is_in_new(gemname)
    debian_name = get_debian_name(gemname)
    rmadison_output = `rmadison -s new -a amd64,all #{debian_name} 2>&1`
    puts "rmadison output is " + rmadison_output
    count = 0
    if rmadison_output.include?("curl:") then
        while rmadison_output.include?("curl:") do
            print "Retrying #"
            puts count
            count = count+1
            rmadison_output = `rmadison -s new -a amd64,all #{debian_name} 2>&1`
            puts "rmadison output is " + rmadison_output
        end
    end
    suite = "NEW"
    status = "NEW"
    begin
        version_crude = rmadison_output.split("|")[1].strip()
    rescue 
        version_crude = "NA"
        suite = "Unpackaged"
        status = "Unpackaged"
    end
    return version_crude,suite,status
end

def is_itp(gemname)
    debian_name = get_debian_name(gemname)
    wnpp_output = `wnpp-check #{debian_name} `
    count = 0
    if wnpp_output.include?("curl:") then
        while wnpp_output.include?("curl:") do
            print "Retrying #"
            puts count
            count = count+1
            wnpp_output = `wnpp-check #{debian_name} `
        end
    end
    suite = "ITP"
    status = "ITP"
    version_crude = "NA"
    if wnpp_output==""
        suite="Unpackaged"
        status = "Unpackaged"
    end
    return version_crude,suite,status
end

def get_color(suite)
    if suite=="Unstable"
        color="success"
    elsif suite=="Experimental"
        color="warning"
    elsif suite=="NEW"
        color="active"
    elsif suite=="ITP"
        color="itp"
    else
        color="danger"
    end
    return color
end

def is_in_debian(gemname)
    debian_name = get_debian_name(gemname)
    puts "######"
    puts debian_name
    puts "\tChecking in unstable"
    (version_crude,suite,status) = is_in_unstable(gemname)
    if version_crude=="NA" then
        puts "\tChecking in experimental"
        (version_crude,suite,status) = is_in_experimental(gemname)
    end
    if version_crude=="NA" then
        puts "\tChecking in NEW"
        (version_crude,suite,status) = is_in_new(gemname)
    end
    if version_crude=="NA" then
        puts "\tChecking for ITP"
        (version_crude,suite,status) = is_itp(gemname)
    end
    if version_crude != "NA" then
        if version_crude =~ /$[0-9]*.*[a-zA-Z]*/
            pos = version_crude.index(/[a-zA-Z]/)
            if pos then
                version_crude = version_crude[0..pos-1]
            end
        end
        if version_crude.include?("-")
            pos = version_crude.index("-")
            version_crude = version_crude[0..pos-1]
        end
        if version_crude.include?("~")
            pos = version_crude.index("~")
            version_crude = version_crude[0..pos-1]
        end
        if version_crude.include?("+")
            pos = version_crude.index("+")
            version_crude = version_crude[0..pos-1]
        end
    end
    color = get_color(suite)
    print "\t"+debian_name
    puts " | " + version_crude + " | " + suite  + " | " + color + " | " + status
    return version_crude,suite,color,status
end


gemfile = File.open('Gemfile')
gemfile_parsed = Gemnasium::Parser.gemfile(gemfile.read())
dep_list = []
req_list = {}
if File.exist?("gemlist") then
    puts "Found Existing Gem List"
    gems_file = File.open("gemlist","r")
    gems_file.each_line do |line|
        data = line.split(",")
        dep_list << data[0]
        req_list[data[0]] = data[1].strip()
    end
    gems_file.close()
else
    gems_file = File.open("gemlist","w")
    puts "Reading Gemfile"
    gemfile_parsed.dependencies.each do |dep|
        dep_list<<dep.name.to_s
        req_list[dep.name.to_s] = dep.requirements_list.join(" ").gsub(","," ")
        gems_file.write(dep.name.to_s+","+dep.requirements_list.join(" ").gsub(","," ").strip()+"\n")
    end
    counter = 0
    puts "Getting Dependency List"
    while true do
        currentgem = dep_list[counter]
        yaml = open("https://rubygems.org/api/v1/gems/#{currentgem}.yaml")
        geminfo = YAML.load(yaml)
        geminfo["dependencies"]["runtime"].each do |k|
            if not dep_list.include?(k["name"]) then
                dep_list << k["name"]
                req_list[k["name"]] = k["requirements"].gsub(",","&#44;")
                gems_file.write(k["name"]+","+req_list[k["name"]].strip()+"\n")
            end
        end
        counter = counter+1
        if counter >= dep_list.length
            break
        end
    end
    gems_file.close()
    puts "Gems List generated"
end
dot_file = File.open('Gemfile.dot','w')
dep_list.each do |dep|
    version,suite,color,status = is_in_debian(dep)
    dot_file.write(dep+","+req_list[dep]+","+version+","+suite+","+color+","+status+"\n")
end
dot_file.close()
